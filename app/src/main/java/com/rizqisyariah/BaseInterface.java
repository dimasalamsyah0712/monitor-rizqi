package com.rizqisyariah;

import android.view.View;

/**
 * Created by Dimas Alamsyah on 9/14/2019.
 */
public interface BaseInterface {
    void btnNext(View view);
    void btnBack(View view);
}
