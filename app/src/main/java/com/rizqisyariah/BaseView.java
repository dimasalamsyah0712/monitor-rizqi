package com.rizqisyariah;

import android.view.View;

/**
 * Created by Dimas Alamsyah on 9/14/2019.
 */
public interface BaseView {
    void onSuccess(String message);
    void onError(String message);
    void onCompleted();
    void onSubmit(View view);
}
