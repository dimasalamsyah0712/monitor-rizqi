package com.rizqisyariah.Prenster;

import com.rizqisyariah.BaseView;
import com.rizqisyariah.Model.PermintaanPembelian;
import com.rizqisyariah.R;
import com.rizqisyariah.View.PermintaanPembelianView;
import com.rizqisyariah.adapter.PermintaanPembelianAdapter;
import com.rizqisyariah.res.ApiClient;
import com.rizqisyariah.res.ApiInterface;

import java.util.ArrayList;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Dimas Alamsyah on 2019-09-28
 * dimasalamsyah0712@gmail.com
 */

public class PermintaanPembelianPresenter implements PermintaanPembelianPresenterInterface{

    private ApiInterface mApiInterface;
    private PermintaanPembelianView baseView;
    private PermintaanPembelianAdapter adapter;
    private ArrayList<PermintaanPembelian> list = new ArrayList<PermintaanPembelian>();

    public PermintaanPembelianPresenter(PermintaanPembelianView baseView) {

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        this.baseView = baseView;
    }

    public void getDataAPI(){
        Observable<ArrayList<PermintaanPembelian>> observable =
                mApiInterface.getPermintaanPembelian()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
        Observer<ArrayList<PermintaanPembelian>> observer = new Observer<ArrayList<PermintaanPembelian>>() {
            @Override
            public void onCompleted() {
                baseView.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                baseView.onError(e.getMessage());
            }

            @Override
            public void onNext(ArrayList<PermintaanPembelian> permintaanPembelians) {
                setData(permintaanPembelians);
            }
        };
        observable.subscribe(observer);
    }

    @Override
    public ArrayList<PermintaanPembelian> getData() {
        return list;
    }

    @Override
    public void setData(ArrayList<PermintaanPembelian> list) {
        this.list = list;
    }

    @Override
    public void onSubmit(PermintaanPembelian permintaanPembelian) {
        Observable<Integer> observable =
                mApiInterface.approvePermintaanPembelian(permintaanPembelian.getId())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());

        Observer<Integer> observer = new Observer<Integer>() {
            @Override
            public void onCompleted() {
                baseView.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                baseView.onError(e.getMessage());
            }

            @Override
            public void onNext(Integer integer) {
                if(integer > 0){
                    baseView.onSuccess( "success" );
                }else{
                    baseView.onError("Failed update data!");
                }
            }
        };

        observable.subscribe(observer);
    }
}
