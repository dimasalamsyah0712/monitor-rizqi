package com.rizqisyariah.Prenster;

import com.rizqisyariah.Model.PermintaanPembelian;

import java.util.ArrayList;

/**
 * Created by Dimas Alamsyah on 2019-09-28
 * dimasalamsyah0712@gmail.com
 */

public interface PermintaanPembelianPresenterInterface {
    ArrayList<PermintaanPembelian> getData();
    void setData(ArrayList<PermintaanPembelian> list);
    void onSubmit(PermintaanPembelian permintaanPembelian);
}
