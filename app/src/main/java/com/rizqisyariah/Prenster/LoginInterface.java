package com.rizqisyariah.Prenster;

/**
 * Created by Dimas Alamsyah on 2019-09-14
 * dimasalamsyah0712@gmail.com
 */

public interface LoginInterface {
    void onLogin(String username, String password);
}
