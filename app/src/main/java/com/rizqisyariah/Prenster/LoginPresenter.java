package com.rizqisyariah.Prenster;

import com.rizqisyariah.Model.User;
import com.rizqisyariah.View.LoginView;

/**
 * Created by Dimas Alamsyah on 2019-09-14
 * dimasalamsyah0712@gmail.com
 */

public class LoginPresenter implements LoginInterface {

    LoginView loginView;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void onLogin(String username, String password) {
        User user = new User(username, password);
        int loginCode = user.isValid();

        if(loginCode == 0) {
            loginView.onLoginError("You must enter your email");
        } else if(loginCode == 1) {
            loginView.onLoginError("You must enter valid email");
        } else if(loginCode == 2) {
            loginView.onLoginError("Password length must be greater than 6");
        } else {
            loginView.onLoginSuccess("Login success");
        }
    }
}
