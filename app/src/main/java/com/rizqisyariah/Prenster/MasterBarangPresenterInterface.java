package com.rizqisyariah.Prenster;

import android.content.Context;
import android.net.Uri;

import com.rizqisyariah.Model.MasterBarang;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Dimas Alamsyah on 9/14/2019.
 */
public interface MasterBarangPresenterInterface {

    void onSubmit(MasterBarang masterBarang);
    void onSubmitWithImage(MasterBarang masterBarang, Uri file, Context context);
}
