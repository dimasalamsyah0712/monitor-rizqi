package com.rizqisyariah.Prenster;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.loader.content.CursorLoader;

import com.rizqisyariah.BaseView;
import com.rizqisyariah.Model.MasterBarang;
import com.rizqisyariah.Model.MasterBarangInterface;
import com.rizqisyariah.Model.PermintaanPembelian;
import com.rizqisyariah.adapter.PermintaanPembelianAdapter;
import com.rizqisyariah.libs.StringUtil;
import com.rizqisyariah.res.ApiClient;
import com.rizqisyariah.res.ApiInterface;

import java.io.File;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.rizqisyariah.BaseActivity.__context;

/**
 * Created by Dimas Alamsyah on 9/14/2019.
 */
public class MasterBarangPresenter implements MasterBarangPresenterInterface {

    private BaseView baseView;
    private ApiInterface mApiInterface;

    public MasterBarangPresenter(BaseView baseView) {

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        this.baseView = baseView;
    }

    @Override
    public void onSubmit(MasterBarang m) {

    }

    @Override
    public void onSubmitWithImage(MasterBarang m, Uri file, Context context) {
        MasterBarang masterBarang = new MasterBarang(m.getNamaBarang(), m.getHargaBeli(),
                m.getHargaJual(), m.getUkuran(), m.getKategori(), m.getGambar(), m.getUrl(), m.getAktif() );

        int valid = masterBarang.isValid();

        if(valid == 1){
            baseView.onError("error");
        }else{
            //Toast.makeText(__context, "test", Toast.LENGTH_SHORT).show();
            saveWithImage(masterBarang, file, context);
        }
    }

    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString);
    }

    private String getRealPathFromURI(Uri contentUri, Context context) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    public void saveWithImage(MasterBarang m, Uri imageUri, Context context){

        File file = new File( getRealPathFromURI(imageUri, context) );
        RequestBody gambarFile = RequestBody.create(MediaType.parse("image/*"), file);
        RequestBody namaBarang = RequestBody.create(MediaType.parse("multipart/form-data"), m.getNamaBarang());
        RequestBody hargaBeli = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(m.getHargaBeli()) );
        RequestBody hargaJual = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(m.getHargaJual()) );
        RequestBody ukuran = RequestBody.create(MediaType.parse("multipart/form-data"), m.getUkuran());
        RequestBody kategori = RequestBody.create(MediaType.parse("multipart/form-data"), m.getKategori());
        RequestBody gambar = RequestBody.create(MediaType.parse("multipart/form-data"), m.getGambar());
        RequestBody urlGambar = RequestBody.create(MediaType.parse("multipart/form-data"), m.getUrl());
        RequestBody aktifBarang = RequestBody.create(MediaType.parse("multipart/form-data"), m.getAktif());

        Observable<String> observable = mApiInterface.saveMasterBarangWithImage(
                namaBarang,
                hargaBeli,
                hargaJual,
                ukuran,
                kategori,
                gambar,
                urlGambar,
                aktifBarang,
                gambarFile)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        Observer<String> saveObserver = new Observer<String>() {
            @Override
            public void onCompleted() {
                baseView.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                baseView.onError(e.toString());
            }

            @Override
            public void onNext(String s) {
                baseView.onSuccess(s);
            }
        };

        observable.subscribe(saveObserver);

    }

    public void save(MasterBarang m){

        Observable<String> saveObservable = mApiInterface.saveMasterBarang(
                m.getNamaBarang(), m.getHargaBeli(), m.getHargaJual(),
                m.getUkuran(), m.getKategori(), m.getGambar(), m.getUrl(), m.getAktif() )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        Observer<String> saveObserver = new Observer<String>() {
            @Override
            public void onCompleted() {
                baseView.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                baseView.onError(e.toString());
            }

            @Override
            public void onNext(String s) {
                baseView.onSuccess(s);
            }
        };

        saveObservable.subscribe(saveObserver);

    }

}
