package com.rizqisyariah;

import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.rizqisyariah.libs.DateTime;

/**
 * Created by Dimas Alamsyah on 9/14/2019.
 */
public class BaseActivity extends AppCompatActivity{

    public final static String BASE_URL = "https://rizqisyariah.com";
    public final static String PATH_MASTER_BARANG = "/gambar_masterbarang";

    public final static String AKTIF_TGL = "9999-12-31 23:59:59";
    public final static int PICK_IMAGE = 1;

    public static Context __context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        __context = this;

    }

    protected void onBtnNext(View view){};

    protected void onBtnBack(View view){};

    protected void setupViews(){};

}
