package com.rizqisyariah.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rizqisyariah.Model.PermintaanPembelian;
import com.rizqisyariah.R;
import com.rizqisyariah.activity.PermintaanPembelianDetailActivity;

import java.util.ArrayList;

/**
 * Created by Dimas Alamsyah on 2019-09-28
 * dimasalamsyah0712@gmail.com
 */

public class PermintaanPembelianAdapter extends RecyclerView.Adapter<PermintaanPembelianAdapter.MyViewHolder> {

    private ArrayList<PermintaanPembelian> list = new ArrayList<PermintaanPembelian>();
    private Context context;

    public PermintaanPembelianAdapter( Context context, ArrayList<PermintaanPembelian> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.__layout_permintaan_pembelian, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        PermintaanPembelian permintaanPembelian = list.get(position);
        holder.txtEmail.setText(permintaanPembelian.getEmail());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PermintaanPembelianDetailActivity.class);
                i.putExtra("data", list.get(position));
                ((Activity) context ).startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtEmail;
        public View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            view = itemView;
            txtEmail = (TextView) itemView.findViewById(R.id.txtEmail);



        }
    }
}
