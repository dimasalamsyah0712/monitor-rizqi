package com.rizqisyariah.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.rizqisyariah.BaseActivity;
import com.rizqisyariah.BaseView;
import com.rizqisyariah.Prenster.PermintaanPembelianPresenter;
import com.rizqisyariah.R;
import com.rizqisyariah.View.PermintaanPembelianView;
import com.rizqisyariah.adapter.PermintaanPembelianAdapter;

public class PermintaanPembelianActivity extends BaseActivity implements PermintaanPembelianView {

    private RecyclerView recyclerView;
    private PermintaanPembelianAdapter adapter;
    private PermintaanPembelianPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permintaan_pembelian);

        presenter = new PermintaanPembelianPresenter(this);
        presenter.getDataAPI();

        setupViews();
    }

    @Override
    protected void setupViews() {
        super.setupViews();

        recyclerView = findViewById(R.id.recyclerView);

    }

    @Override
    public void onSuccess(String message) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onCompleted() {
        adapter = new PermintaanPembelianAdapter(__context, presenter.getData());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onSubmit(View view) {

    }
}
