package com.rizqisyariah.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.rizqisyariah.BaseActivity;
import com.rizqisyariah.BaseView;
import com.rizqisyariah.Model.MasterBarang;
import com.rizqisyariah.Prenster.MasterBarangPresenter;
import com.rizqisyariah.R;
import com.rizqisyariah.View.MasterBarangView;
import com.rizqisyariah.libs.StringUtil;
import com.rizqisyariah.res.ApiClient;
import com.rizqisyariah.res.ApiInterface;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class MasterBarangAddActivity extends BaseActivity implements BaseView, MasterBarangView {

    private EditText etNamaBarang, etHargaBeli, etHargaJual, etSize, etKategori;
    private EditText etLink;
    private TextInputLayout tiNamaBarang;
    private Button btnNext;
    private ImageView imageView;
    private MasterBarangPresenter masterBarangPresenter;
    private Uri uri;
    private String generateLink;
    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master_barang_add);

        setupViews();
        masterBarangPresenter = new MasterBarangPresenter(this);

    }

    @Override
    protected void setupViews() {
        super.setupViews();

        etNamaBarang = findViewById(R.id.etNamaBarang);
        imageView = findViewById(R.id.imageView);
        etLink = findViewById(R.id.etLink);
        etHargaBeli = findViewById(R.id.etHargaBeli);
        etHargaJual = findViewById(R.id.etHargaJual);
        etSize = findViewById(R.id.etSize);
        etKategori = findViewById(R.id.etKategori);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
    }

    @Override
    public void onSuccess(String message) {
        Toasty.success(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String message) {
        Toasty.error(this, message, Toast.LENGTH_SHORT).show();
        pDialog.dismiss();
    }

    @Override
    public void onCompleted() {
        etLink.setText(BASE_URL+"/flink/"+generateLink);
        pDialog.dismiss();
    }

    @Override
    public void onSubmit(View view) {

    }

    @Override
    public void onOpenImage(View view) {
        pickImage();
    }

    @Override
    public void onSubmitWithImage(View view) {

        pDialog.show();

        generateLink = StringUtil.generateCode();

        MasterBarang masterBarang = new MasterBarang(etNamaBarang.getText().toString(), Integer.valueOf(etHargaBeli.getText().toString()), Integer.valueOf(etHargaJual.getText().toString()),
                etSize.getText().toString(), etKategori.getText().toString(), "", generateLink, AKTIF_TGL);
        masterBarangPresenter.onSubmitWithImage(masterBarang, uri, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        uri = data.getData();
        if (uri != null) {
            try {
                setImageFromBitmap(uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            // todo do something with bitmap
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 1){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            }
        }

    }

    private void setImageFromBitmap(Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
        imageView.setImageBitmap(bitmap);
    }

    private void pickImage() {
        if (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.INTERNAL_CONTENT_URI
            );
            intent.setType("image/*");
            intent.putExtra("crop", "true");
            intent.putExtra("scale", true);
            intent.putExtra("aspectX", 16);
            intent.putExtra("aspectY", 9);
            startActivityForResult(intent, PICK_IMAGE);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, 1);
            }
        }
    }
}
