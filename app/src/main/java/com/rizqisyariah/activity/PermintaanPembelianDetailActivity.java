package com.rizqisyariah.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rizqisyariah.BaseActivity;
import com.rizqisyariah.BaseView;
import com.rizqisyariah.Model.PermintaanPembelian;
import com.rizqisyariah.Prenster.PermintaanPembelianPresenter;
import com.rizqisyariah.R;
import com.rizqisyariah.View.PermintaanPembelianView;
import com.squareup.picasso.Picasso;

import es.dmoral.toasty.Toasty;

public class PermintaanPembelianDetailActivity extends BaseActivity implements PermintaanPembelianView {

    private TextView tvNamaBarang;
    private TextView tvUkuran;
    private TextView tvHarga;
    private TextView tvQty;
    private TextView tvEmail;
    private TextView tvNama;
    private TextView tvAlamat;
    private TextView tvNoHP;
    private TextView tvKeterangan;
    private TextView tvTipePembayaran;
    private TextView tvAngsuran;
    private TextView tvTanggalJatuhTempo;
    private ImageView imageView;
    private PermintaanPembelian permintaanPembelian;
    private PermintaanPembelianPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permintaan_pembelian_detail);

        setupViews();
    }

    @Override
    protected void setupViews() {
        super.setupViews();

        tvNamaBarang = findViewById(R.id.tvNamaBarang);
        tvUkuran = findViewById(R.id.tvUkuran);
        tvHarga = findViewById(R.id.tvHarga);
        tvQty = findViewById(R.id.tvQty);
        tvEmail = findViewById(R.id.tvEmail);
        tvNama = findViewById(R.id.tvNama);
        tvAlamat = findViewById(R.id.tvAlamat);
        tvNoHP = findViewById(R.id.tvNoHP);
        tvKeterangan = findViewById(R.id.tvKeterangan);
        tvTipePembayaran = findViewById(R.id.tvTipePembayaran);
        tvAngsuran = findViewById(R.id.tvAngsuran);
        tvTanggalJatuhTempo = findViewById(R.id.tvTanggalJatuhTempo);
        imageView = findViewById(R.id.ivGambarBarang);

        permintaanPembelian = (PermintaanPembelian) getIntent().getSerializableExtra("data");
        presenter = new PermintaanPembelianPresenter(this);

        tvNamaBarang.setText(permintaanPembelian.getNama_barang());
        tvUkuran.setText(permintaanPembelian.getUkuran());
        tvHarga.setText(permintaanPembelian.getHarga());
        tvQty.setText(permintaanPembelian.getQty());
        tvEmail.setText(permintaanPembelian.getEmail());
        tvNama.setText(permintaanPembelian.getNama_barang());
        tvAlamat.setText(permintaanPembelian.getAlamat());
        tvNoHP.setText(permintaanPembelian.getNo_hp());
        tvKeterangan.setText(permintaanPembelian.getKeterangan());
        tvTipePembayaran.setText(permintaanPembelian.getTipe_pembayaran());
        tvAngsuran.setText(permintaanPembelian.getAngsuran());
        tvTanggalJatuhTempo.setText(permintaanPembelian.getJatuh_tempo());

        Picasso.get()
                .load(BASE_URL + PATH_MASTER_BARANG + "/" + permintaanPembelian.getGambar())
                .resize(80, 80)
                .centerCrop()
                .into(imageView);
    }

    @Override
    public void onSuccess(String message) {
        Toasty.success(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String message) {
        Toasty.success(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onSubmit(View view) {
        presenter.onSubmit(permintaanPembelian);
    }
}
