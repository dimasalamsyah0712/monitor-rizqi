package com.rizqisyariah.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.rizqisyariah.BaseActivity;
import com.rizqisyariah.Model.PermintaanPembelian;
import com.rizqisyariah.R;

public class MenuActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void onClickMasterBarang(View view){
        Intent intent = new Intent(this, MasterBarangAddActivity.class);
        startActivity(intent);
    }

    public void onClickPermintaanPembelian(View view){
        Intent intent = new Intent(this, PermintaanPembelianActivity.class);
        startActivity(intent);
    }

}
