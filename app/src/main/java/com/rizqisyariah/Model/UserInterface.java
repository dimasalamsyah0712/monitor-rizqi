package com.rizqisyariah.Model;

/**
 * Created by Dimas Alamsyah on 2019-09-14
 * dimasalamsyah0712@gmail.com
 */

public interface UserInterface {
    int isValid();
}
