package com.rizqisyariah.Model;

import android.text.TextUtils;

/**
 * Created by Dimas Alamsyah on 9/14/2019.
 */
public class MasterBarang implements MasterBarangInterface {

    private String namaBarang;
    private double hargaBeli;
    private double hargaJual;
    private String ukuran;
    private String kategori;
    private String gambar;
    private String url;
    private String aktif;

    public MasterBarang(String namaBarang, double hargaBeli, double hargaJual,
                        String ukuran, String kategori, String gambar, String url, String aktif) {
        this.namaBarang = namaBarang;
        this.hargaBeli = hargaBeli;
        this.hargaJual = hargaJual;
        this.ukuran = ukuran;
        this.kategori = kategori;
        this.gambar = gambar;
        this.url = url;
        this.aktif = aktif;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAktif() {
        return aktif;
    }

    public void setAktif(String aktif) {
        this.aktif = aktif;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public double getHargaBeli() {
        return hargaBeli;
    }

    public void setHargaBeli(double hargaBeli) {
        this.hargaBeli = hargaBeli;
    }

    public double getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(double hargaJual) {
        this.hargaJual = hargaJual;
    }

    public String getUkuran() {
        return ukuran;
    }

    public void setUkuran(String ukuran) {
        this.ukuran = ukuran;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    @Override
    public int isValid() {
        if(TextUtils.isEmpty( getNamaBarang() )) {
            return 1;
        }

        return -1;
    }
}
