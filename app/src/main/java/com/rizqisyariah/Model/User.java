package com.rizqisyariah.Model;

import android.text.TextUtils;

/**
 * Created by Dimas Alamsyah on 2019-09-14
 * dimasalamsyah0712@gmail.com
 */

public class User implements UserInterface {

    private String id;
    private String username;
    private String email;
    private String password;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int isValid() {

        if(TextUtils.isEmpty( getUsername() )) {
            return 1;
        }else if(TextUtils.isEmpty( getPassword() )){
            return 2;
        } else if(getPassword().length() <= 3) {
            return 3;
        }

        return -1;
    }
}
