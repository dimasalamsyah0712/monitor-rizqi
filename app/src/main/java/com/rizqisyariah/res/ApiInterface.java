package com.rizqisyariah.res;

import com.rizqisyariah.Model.PermintaanPembelian;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Dimas Alamsyah on 8/26/2019.
 */


public interface ApiInterface {

    @Multipart
    @POST("masterbarang")
    Observable<String> saveMasterBarang(
            @Field("nama_barang") String nama_barang,
            @Field("harga_jual") double harga_jual,
            @Field("harga_beli") double harga_beli,
            @Field("ukuran") String ukuran,
            @Field("kategori") String kategori,
            @Field("gambar") String gambar,
            @Field("url") String url,
            @Field("aktif") String aktif
    );

    //https://stackoverflow.com/questions/34562950/post-multipart-form-data-using-retrofit-2-0-including-image
    @Multipart
    @POST("masterbarang")
    Observable<String> saveMasterBarangWithImage(
            @Part("nama_barang") RequestBody nama_barang,
            @Part("harga_jual") RequestBody harga_jual,
            @Part("harga_beli") RequestBody harga_beli,
            @Part("ukuran") RequestBody ukuran,
            @Part("kategori") RequestBody kategori,
            @Part("gambar") RequestBody gambar,
            @Part("url") RequestBody url,
            @Part("aktif") RequestBody aktif,
            @Part("image\"; filename=\"myfile.jpg\" ") RequestBody file
    );

    @Multipart
    @POST("/upload")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file, @Part("name") RequestBody requestBody);

    @Multipart
    @POST("/upload")
    Observable<String> onFileUpload(@Part("username") RequestBody mUserName,
                                    @Part("email") RequestBody mEmail,
                                    @Part("image\"; filename=\"myfile.jpg\" ") RequestBody file);


    @GET("/permintaan_pembelian")
    Observable<ArrayList<PermintaanPembelian>> getPermintaanPembelian();

    @POST("/permintaan_pembelian")
    Observable<Integer> approvePermintaanPembelian(@Query("id") String id);

}
