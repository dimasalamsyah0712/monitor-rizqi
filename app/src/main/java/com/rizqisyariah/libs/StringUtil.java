package com.rizqisyariah.libs;

import java.security.SecureRandom;

/**
 * Created by Dimas Alamsyah on 9/15/2019.
 */
public class StringUtil {

    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";

    private static final String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
    private static SecureRandom random = new SecureRandom();

    public static String generateCode(){

        DateTime dateTime = new DateTime();
        String[] day = {"St", "Du", "Tg", "Em", "Lm", "En", "Tj", "Dl", "Sb", "Sp",
                        "St1", "Du1", "Tg1", "Em1", "Lm1", "En1", "Tj1", "Dl1", "Sb1", "Sp1",
                        "St2", "Du2", "Tg2", "Em2", "Lm2", "En2", "Tj2", "Dl2", "Sb2", "Sp2",
                        "St3"};
        String[] bln = {"Jr", "Fb", "Mr", "Ap", "Me", "Jn", "Jy", "Ag", "Sp", "Ok", "Nv", "Ds"};
        String[] Int = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
                        "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                        "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
                        "31"};

        String blnGnrt= "";
        String dayGnrt= "";

        for(int i = 0; i < 12; i++){

            if(dateTime.getFormat("MM").equals( Int[i] )){
                blnGnrt = bln[i];
            }

        }
        for(int i = 0; i <= 30; i++){

            if(dateTime.getFormat("dd").equals( Int[i] )){
                dayGnrt = day[i];
            }

        }


        String hash = generateRandomString(2)+
                dateTime.getFormat("yy") +
                generateRandomString(2) +
                blnGnrt +
                generateRandomString(2)+
                dayGnrt +
                generateRandomString(2);

        return hash;
    }

    public static String generateRandomString(int length) {
        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {

            // 0-62 (exclusive), random returns 0-61
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

            // debug
            System.out.format("%d\t:\t%c%n", rndCharAt, rndChar);

            sb.append(rndChar);

        }

        return sb.toString();

    }

}
