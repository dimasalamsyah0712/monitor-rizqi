package com.rizqisyariah.libs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Dimas Alamsyah on 9/15/2019.
 */
public class DateTime {

    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;

    public static final String dateFormat_yyMMdd = "yyyyMMdd";
    public static final String dateFormat_yy = "yy";
    public static final String dateFormat_MM = "MM";
    public static final String dateFormat_dd = "dd";

    public DateTime() {
        this.calendar = Calendar.getInstance();
    }

    public String getFormat(String dateFormat){
        simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);

        return simpleDateFormat.format(calendar.getTime());
    }



}
