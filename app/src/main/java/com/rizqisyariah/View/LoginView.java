package com.rizqisyariah.View;

/**
 * Created by Dimas Alamsyah on 2019-09-14
 * dimasalamsyah0712@gmail.com
 */

public interface LoginView {
    void onLoginSuccess(String message);
    void onLoginError(String message);
}
