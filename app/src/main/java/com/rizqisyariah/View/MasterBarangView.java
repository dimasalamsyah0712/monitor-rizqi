package com.rizqisyariah.View;

import android.view.View;

/**
 * Created by Dimas Alamsyah on 9/21/2019.
 */
public interface MasterBarangView {
    void onOpenImage(View view);
    void onSubmitWithImage(View view);
}
