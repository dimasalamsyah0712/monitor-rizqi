package com.rizqisyariah.View;

import android.view.View;

/**
 * Created by Dimas Alamsyah on 2019-10-03
 * dimasalamsyah0712@gmail.com
 */

public interface PermintaanPembelianView {
    void onSuccess(String message);
    void onError(String message);
    void onCompleted();
    void onSubmit(View view);
}
